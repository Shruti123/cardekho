
// hamburger menu icon click to close button
// function myFunction(x) {
//     x.classList.toggle("change-hamburger");
// }

function openNav() {
   document.getElementById("canvasMenu").style.width = "100%";
   document.body.style.backgroundColor = "rgba(255,255,255,0.8)";
}

function closeNav() {
   document.getElementById("canvasMenu").style.width = "0";
   document.body.style.backgroundColor = "white";
}

$(document).ready(function(){
   $('#canvasMenu ul li').click(function(){
       $(this).siblings().removeClass('active');
       $(this).addClass('active');
       $('#canvasMenu').css('width' , '0');  
       
   });
});
// $(window).keypress(function(e) {
//     console.log("keycode",e) 
//     if (e.keyCode == 0 || e.keyCode == 32) {// `0` works in mozilla and `32` in other browsers
//     ps1.navigate('next');
//     ps2.navigate('next');
//     ps.navigate('next');}
    
// });

$(document).ready(function(){
    $(window).keydown(function(e){ 
       console.log(e);
       var PP=$.fn.pagepiling
        if (e.keyCode === 38 || e.keyCode === 33) { 
            PP.moveSectionUp();
            $('.up-arr').addClass('active');
            $('.btm-text').text("Up");
            }  
         else{
            $('.up-arr').removeClass('active'); 
         }  
         
         if (e.keyCode  === 40 || e.keyCode === 34) { 
            PP.moveSectionDown();
            $('.down-arr').addClass('active');
            $('.btm-text').text("Down");
            }  
         else{
            $('.down-arr').removeClass('active'); 
         } 
         
         if (e.keyCode  === 37) { 
            
            $('.left-arr').addClass('active');
            $('.btm-text').text("Left");
            }  
         else{
            $('.left-arr').removeClass('active'); 
         } 
         if (e.keyCode  ===39) { 
            
            $('.right-arr').addClass('active');
            $('.btm-text').text("Right");
            }  
         else{
            $('.right-arr').removeClass('active'); 
         } 
    });
  });
  $(window).bind('DOMMouseScroll', function(e){
   if(e.originalEvent.detail > 0) {
      $('.up-arr').removeClass('active'); 
      $('.down-arr').addClass('active');
      $('.btm-text').text("Down");
   }else {
      $('.down-arr').removeClass('active'); 
      $('.up-arr').addClass('active');
      $('.btm-text').text("Up");
   }
   return false;
});

$(window).bind('mousewheel', function(e){
   if(e.originalEvent.wheelDelta < 0) {
      $('.up-arr').removeClass('active'); 
      $('.down-arr').addClass('active');
      $('.btm-text').text("Down");
   }else {
      $('.down-arr').removeClass('active'); 
      $('.up-arr').addClass('active');
      $('.btm-text').text("Up");
   }
   return false;
});
$(document).ready(function(){
particlesJS("particles-js", {
   "particles": {
       "number": {
           "value": 355,
           "density": {
               "enable": true,
               "value_area": 789.1476416322727
           }
       },
       "color": {
           "value": "#000000"
       },
       "shape": {
           "type": "circle",
           "stroke": {
               "width": 0,
               "color": "#000000"
           },
           "polygon": {
               "nb_sides": 5
           }
       },
       "opacity": {
           "value": 0.48927153781200905,
           "random": false,
           "anim": {
               "enable": true,
               "speed": 0.2,
               "opacity_min": 0.1,
               "sync": false
           }
       },
       "size": {
           "value": 2,
           "random": true,
           "anim": {
               "enable": true,
               "speed": 2,
               "size_min": 0,
               "sync": false
           }
       },
       "line_linked": {
           "enable": false,
           "distance": 150,
           "color": "#000",
           "opacity": 0.4,
           "width": 1
       },
       "move": {
           "enable": true,
           "speed": 2,
           "direction": "none",
           "random": true,
           "straight": false,
           "out_mode": "out",
           "bounce": false,
           "attract": {
               "enable": false,
               "rotateX": 600,
               "rotateY": 1200
           }
       }
   },
   "interactivity": {
       "detect_on": "canvas",
       "events": {
           "onhover": {
               "enable": true,
               "mode": "bubble"
           },
           "onclick": {
               "enable": true,
               "mode": "push"
           },
           "resize": true
       },
       "modes": {
           "grab": {
               "distance": 400,
               "line_linked": {
                   "opacity": 1
               }
           },
           "bubble": {
               "distance": 83.91608391608392,
               "size": 1,
               "duration": 3,
               "opacity": 8,
               "speed": 3
           },
           "repulse": {
               "distance": 200,
               "duration": 0.4
           },
           "push": {
               "particles_nb": 4
           },
           "remove": {
               "particles_nb": 2
           }
       }
   },
   "retina_detect": true
});
particlesJS("particles-js2", {
   "particles": {
       "number": {
           "value": 355,
           "density": {
               "enable": true,
               "value_area": 789.1476416322727
           }
       },
       "color": {
           "value": "#000000"
       },
       "shape": {
           "type": "circle",
           "stroke": {
               "width": 0,
               "color": "#000000"
           },
           "polygon": {
               "nb_sides": 5
           }
       },
       "opacity": {
           "value": 0.48927153781200905,
           "random": false,
           "anim": {
               "enable": true,
               "speed": 0.2,
               "opacity_min": 0.1,
               "sync": false
           }
       },
       "size": {
           "value": 2,
           "random": true,
           "anim": {
               "enable": true,
               "speed": 2,
               "size_min": 0,
               "sync": false
           }
       },
       "line_linked": {
           "enable": false,
           "distance": 150,
           "color": "#000",
           "opacity": 0.4,
           "width": 1
       },
       "move": {
           "enable": true,
           "speed": 2,
           "direction": "none",
           "random": true,
           "straight": false,
           "out_mode": "out",
           "bounce": false,
           "attract": {
               "enable": false,
               "rotateX": 600,
               "rotateY": 1200
           }
       }
   },
   "interactivity": {
       "detect_on": "canvas",
       "events": {
           "onhover": {
               "enable": true,
               "mode": "bubble"
           },
           "onclick": {
               "enable": true,
               "mode": "push"
           },
           "resize": true
       },
       "modes": {
           "grab": {
               "distance": 400,
               "line_linked": {
                   "opacity": 1
               }
           },
           "bubble": {
               "distance": 83.91608391608392,
               "size": 1,
               "duration": 3,
               "opacity": 8,
               "speed": 3
           },
           "repulse": {
               "distance": 200,
               "duration": 0.4
           },
           "push": {
               "particles_nb": 4
           },
           "remove": {
               "particles_nb": 2
           }
       }
   },
   "retina_detect": true
});
particlesJS("particles-js3", {
   "particles": {
       "number": {
           "value": 355,
           "density": {
               "enable": true,
               "value_area": 789.1476416322727
           }
       },
       "color": {
           "value": "#000000"
       },
       "shape": {
           "type": "circle",
           "stroke": {
               "width": 0,
               "color": "#000000"
           },
           "polygon": {
               "nb_sides": 5
           }
       },
       "opacity": {
           "value": 0.48927153781200905,
           "random": false,
           "anim": {
               "enable": true,
               "speed": 0.2,
               "opacity_min": 0.1,
               "sync": false
           }
       },
       "size": {
           "value": 2,
           "random": true,
           "anim": {
               "enable": true,
               "speed": 2,
               "size_min": 0,
               "sync": false
           }
       },
       "line_linked": {
           "enable": false,
           "distance": 150,
           "color": "#000",
           "opacity": 0.4,
           "width": 1
       },
       "move": {
           "enable": true,
           "speed": 2,
           "direction": "none",
           "random": true,
           "straight": false,
           "out_mode": "out",
           "bounce": false,
           "attract": {
               "enable": false,
               "rotateX": 600,
               "rotateY": 1200
           }
       }
   },
   "interactivity": {
       "detect_on": "canvas",
       "events": {
           "onhover": {
               "enable": true,
               "mode": "bubble"
           },
           "onclick": {
               "enable": true,
               "mode": "push"
           },
           "resize": true
       },
       "modes": {
           "grab": {
               "distance": 400,
               "line_linked": {
                   "opacity": 1
               }
           },
           "bubble": {
               "distance": 83.91608391608392,
               "size": 1,
               "duration": 3,
               "opacity": 8,
               "speed": 3
           },
           "repulse": {
               "distance": 200,
               "duration": 0.4
           },
           "push": {
               "particles_nb": 4
           },
           "remove": {
               "particles_nb": 2
           }
       }
   },
   "retina_detect": true
});
particlesJS("particles-js4", {
   "particles": {
       "number": {
           "value": 355,
           "density": {
               "enable": true,
               "value_area": 789.1476416322727
           }
       },
       "color": {
           "value": "#000000"
       },
       "shape": {
           "type": "circle",
           "stroke": {
               "width": 0,
               "color": "#000000"
           },
           "polygon": {
               "nb_sides": 5
           }
       },
       "opacity": {
           "value": 0.48927153781200905,
           "random": false,
           "anim": {
               "enable": true,
               "speed": 0.2,
               "opacity_min": 0.1,
               "sync": false
           }
       },
       "size": {
           "value": 2,
           "random": true,
           "anim": {
               "enable": true,
               "speed": 2,
               "size_min": 0,
               "sync": false
           }
       },
       "line_linked": {
           "enable": false,
           "distance": 150,
           "color": "#000",
           "opacity": 0.4,
           "width": 1
       },
       "move": {
           "enable": true,
           "speed": 2,
           "direction": "none",
           "random": true,
           "straight": false,
           "out_mode": "out",
           "bounce": false,
           "attract": {
               "enable": false,
               "rotateX": 600,
               "rotateY": 1200
           }
       }
   },
   "interactivity": {
       "detect_on": "canvas",
       "events": {
           "onhover": {
               "enable": true,
               "mode": "bubble"
           },
           "onclick": {
               "enable": true,
               "mode": "push"
           },
           "resize": true
       },
       "modes": {
           "grab": {
               "distance": 400,
               "line_linked": {
                   "opacity": 1
               }
           },
           "bubble": {
               "distance": 83.91608391608392,
               "size": 1,
               "duration": 3,
               "opacity": 8,
               "speed": 3
           },
           "repulse": {
               "distance": 200,
               "duration": 0.4
           },
           "push": {
               "particles_nb": 4
           },
           "remove": {
               "particles_nb": 2
           }
       }
   },
   "retina_detect": true
});
particlesJS("particles-js5", {
   "particles": {
       "number": {
           "value": 355,
           "density": {
               "enable": true,
               "value_area": 789.1476416322727
           }
       },
       "color": {
           "value": "#000000"
       },
       "shape": {
           "type": "circle",
           "stroke": {
               "width": 0,
               "color": "#000000"
           },
           "polygon": {
               "nb_sides": 5
           }
       },
       "opacity": {
           "value": 0.48927153781200905,
           "random": false,
           "anim": {
               "enable": true,
               "speed": 0.2,
               "opacity_min": 0.1,
               "sync": false
           }
       },
       "size": {
           "value": 2,
           "random": true,
           "anim": {
               "enable": true,
               "speed": 2,
               "size_min": 0,
               "sync": false
           }
       },
       "line_linked": {
           "enable": false,
           "distance": 150,
           "color": "#000",
           "opacity": 0.4,
           "width": 1
       },
       "move": {
           "enable": true,
           "speed": 2,
           "direction": "none",
           "random": true,
           "straight": false,
           "out_mode": "out",
           "bounce": false,
           "attract": {
               "enable": false,
               "rotateX": 600,
               "rotateY": 1200
           }
       }
   },
   "interactivity": {
       "detect_on": "canvas",
       "events": {
           "onhover": {
               "enable": true,
               "mode": "bubble"
           },
           "onclick": {
               "enable": true,
               "mode": "push"
           },
           "resize": true
       },
       "modes": {
           "grab": {
               "distance": 400,
               "line_linked": {
                   "opacity": 1
               }
           },
           "bubble": {
               "distance": 83.91608391608392,
               "size": 1,
               "duration": 3,
               "opacity": 8,
               "speed": 3
           },
           "repulse": {
               "distance": 200,
               "duration": 0.4
           },
           "push": {
               "particles_nb": 4
           },
           "remove": {
               "particles_nb": 2
           }
       }
   },
   "retina_detect": true
});
particlesJS("particles-js6", {
   "particles": {
       "number": {
           "value": 355,
           "density": {
               "enable": true,
               "value_area": 789.1476416322727
           }
       },
       "color": {
           "value": "#000000"
       },
       "shape": {
           "type": "circle",
           "stroke": {
               "width": 0,
               "color": "#000000"
           },
           "polygon": {
               "nb_sides": 5
           }
       },
       "opacity": {
           "value": 0.48927153781200905,
           "random": false,
           "anim": {
               "enable": true,
               "speed": 0.2,
               "opacity_min": 0.1,
               "sync": false
           }
       },
       "size": {
           "value": 2,
           "random": true,
           "anim": {
               "enable": true,
               "speed": 2,
               "size_min": 0,
               "sync": false
           }
       },
       "line_linked": {
           "enable": false,
           "distance": 150,
           "color": "#000",
           "opacity": 0.4,
           "width": 1
       },
       "move": {
           "enable": true,
           "speed": 2,
           "direction": "none",
           "random": true,
           "straight": false,
           "out_mode": "out",
           "bounce": false,
           "attract": {
               "enable": false,
               "rotateX": 600,
               "rotateY": 1200
           }
       }
   },
   "interactivity": {
       "detect_on": "canvas",
       "events": {
           "onhover": {
               "enable": true,
               "mode": "bubble"
           },
           "onclick": {
               "enable": true,
               "mode": "push"
           },
           "resize": true
       },
       "modes": {
           "grab": {
               "distance": 400,
               "line_linked": {
                   "opacity": 1
               }
           },
           "bubble": {
               "distance": 83.91608391608392,
               "size": 1,
               "duration": 3,
               "opacity": 8,
               "speed": 3
           },
           "repulse": {
               "distance": 200,
               "duration": 0.4
           },
           "push": {
               "particles_nb": 4
           },
           "remove": {
               "particles_nb": 2
           }
       }
   },
   "retina_detect": true
});
particlesJS("particles-js7", {
   "particles": {
       "number": {
           "value": 355,
           "density": {
               "enable": true,
               "value_area": 789.1476416322727
           }
       },
       "color": {
           "value": "#000000"
       },
       "shape": {
           "type": "circle",
           "stroke": {
               "width": 0,
               "color": "#000000"
           },
           "polygon": {
               "nb_sides": 5
           }
       },
       "opacity": {
           "value": 0.48927153781200905,
           "random": false,
           "anim": {
               "enable": true,
               "speed": 0.2,
               "opacity_min": 0.1,
               "sync": false
           }
       },
       "size": {
           "value": 2,
           "random": true,
           "anim": {
               "enable": true,
               "speed": 2,
               "size_min": 0,
               "sync": false
           }
       },
       "line_linked": {
           "enable": false,
           "distance": 150,
           "color": "#000",
           "opacity": 0.4,
           "width": 1
       },
       "move": {
           "enable": true,
           "speed": 2,
           "direction": "none",
           "random": true,
           "straight": false,
           "out_mode": "out",
           "bounce": false,
           "attract": {
               "enable": false,
               "rotateX": 600,
               "rotateY": 1200
           }
       }
   },
   "interactivity": {
       "detect_on": "canvas",
       "events": {
           "onhover": {
               "enable": true,
               "mode": "bubble"
           },
           "onclick": {
               "enable": true,
               "mode": "push"
           },
           "resize": true
       },
       "modes": {
           "grab": {
               "distance": 400,
               "line_linked": {
                   "opacity": 1
               }
           },
           "bubble": {
               "distance": 83.91608391608392,
               "size": 1,
               "duration": 3,
               "opacity": 8,
               "speed": 3
           },
           "repulse": {
               "distance": 200,
               "duration": 0.4
           },
           "push": {
               "particles_nb": 4
           },
           "remove": {
               "particles_nb": 2
           }
       }
   },
   "retina_detect": true
});
});